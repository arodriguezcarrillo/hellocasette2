package com.contoso.hellocasette.dal;

/**
 * Created by alberto on 7/2/15.
 */
public class RestParameter {
    private String _key;
    private String _value;

    public RestParameter(String key, String value){
        this._key = key;
        this._value = value;
    }

    public String get_key() {
        return _key;
    }

    public String get_value() {
        return _value;
    }
}
