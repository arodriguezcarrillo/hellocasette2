package com.contoso.hellocasette.dal;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

public class RestCallTask extends 
		AsyncTask<RestParameter,
			Void,
			String>{
	private Integer _identifier;
	private String _url;
	private IRestCallTaskCallback _callback;
	private CallType _callType;
	private final static String BaseUrl =
		"http://vps111149.ovh.net/hellocasette/service/";
    private Integer _statusCode;
	
	public RestCallTask(
			Integer identifier, 
			String url,
			IRestCallTaskCallback callback,
			CallType type){
		this._identifier = identifier;
		this._url = BaseUrl + url;
		this._callback = callback;
		this._callType = type;
	}

	@Override
	protected String doInBackground(
            RestParameter... params) {
		String result = "";
		try{
			HttpClient client = new DefaultHttpClient();
			
			/*
			 * [
			 * 	{clave: aaaa
			 * 	 valor: bbbb
			 * 	}
			 * 	{clave: aaaa
			 * 	 valor: bbbb
			 * 	}
			 * ]
			 */
			List<NameValuePair> pars = null;
	        if (params != null && params.length > 0){
	            pars = new LinkedList<NameValuePair>();
	            for (RestParameter item : params){
	            	pars.add(new BasicNameValuePair(item.get_key(), item.get_value()));
	            }
	        }
	
	        HttpRequestBase call = null;
	
	        if (this._callType == CallType.GET){
	            if (pars != null)
	                this._url += "?" + URLEncodedUtils.format(pars, "utf-8");
	            call = new HttpGet(this._url);
	        } else if (this._callType == CallType.POST){
	            HttpPost httppost = new HttpPost(this._url);
	            if (pars != null)
	                httppost.setEntity(new UrlEncodedFormEntity(pars));
	
	            call = httppost;
	        } else if (this._callType == CallType.PUT){
	            HttpPut httpput = new HttpPut(this._url);
	            if (pars != null)
	            	httpput.setEntity(new UrlEncodedFormEntity(pars));
	
	            call = httpput;
	        } else if (this._callType == CallType.DELETE){
	            if (pars != null)
	                this._url += 
	                	"?" + URLEncodedUtils.format(pars, "utf-8");
	            call = new HttpDelete(this._url);
	        }
	
	        Log.d("default", this._url);
        
        	HttpResponse response = client.execute(call);
            this._statusCode = response.getStatusLine().getStatusCode();
        	HttpEntity entity = response.getEntity();
        	
        	InputStream instream = entity.getContent();
            result = convertStreamToString(instream);
            Log.d("default", result);
        } catch (Exception excep){
        	Log.d("ERROR", excep.getMessage());
        }
        
        return result;
	}
	
	@Override
	protected void onPostExecute(String result) {
        if (this._statusCode == 200) {
            this._callback.Executed(this._identifier,
                    result, false);
        } else if(this._statusCode == 403) {
            this._callback.Executed(this._identifier,
                    "User not authorized", true);
        } else if (this._statusCode == 404){
            this._callback.Executed(this._identifier,
                    "Not found error", true);
        } else {
            this._callback.Executed(this._identifier,
                    "Internal error", true);
        }
	}
	
	private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

	/*
	 * El callback va a tener un método con
	 * tres parámetros, que serán:
	 * 	Identificador de la llamada(integer)
	 * 	Resultado ( string)
	 *  Error (booleano)
	 */
	public interface IRestCallTaskCallback{
		void Executed(Integer id, 
				String result, Boolean error);
	}
}
