package com.contoso.hellocasette.dal;

public enum CallType {
	GET(0), POST(1), PUT(2), DELETE(3);
	
	Integer _value;
	
	CallType(Integer value){
		this._value = value;
	}
}
