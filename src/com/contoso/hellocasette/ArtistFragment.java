package com.contoso.hellocasette;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.contoso.hellocasette.businessobjects.AlbumsBO;
import com.contoso.hellocasette.businessobjects.ArtistsBO;
import com.contoso.hellocasette.businessobjects.IAlbumsBO;
import com.contoso.hellocasette.businessobjects.IArtistsBO;
import com.contoso.hellocasette.crosscutting.Album;
import com.contoso.hellocasette.crosscutting.Artist;
import com.contoso.hellocasette.crosscutting.helpers.ImageLoaderTask;

import java.util.ArrayList;


public class ArtistFragment extends Fragment
        implements IAlbumsBO.IAlbumsBOCallback, IArtistsBO.IArtistBOCallback {

    ProgressDialog _progressDialog;
    AlbumsBO _businessObjects;
    ArtistsBO _artistBusinessObjects;
    ArrayAdapter<Album> _adapter;
    IArtistFragmentCallback _callback;
    Integer _artistId;
    View _view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        this._view = inflater.inflate(R.layout.fragment_artist, null);

        this.InitializeAdapter();
        this.InitializeBusinessObjects();
        Bundle bundle = getArguments();
        this._artistId = bundle.getInt("id");
        this.LoadInitialData();

        return this._view;
    }

    private void InitializeBusinessObjects(){
        this._businessObjects = new AlbumsBO(getActivity(), this);
        this._artistBusinessObjects = new ArtistsBO(getActivity(), this);
    }

    private void LoadInitialData(){
        this.StartProgressDialog("Cargando álbumes y datos del artista");
        this._businessObjects.Albums(this._artistId);
        this._artistBusinessObjects.Artist(this._artistId);
    }

    private void InitializeAdapter(){
        this._adapter = new ArrayAdapter<Album>(getActivity(),
                R.layout.grid_item, new ArrayList<Album>()){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getActivity().getLayoutInflater().inflate(R.layout.grid_item,
                            null);

                TextView tvTitle = (TextView)convertView.findViewById(R.id.tvRowTitle);
                TextView tvSubtitle = (TextView)convertView.findViewById(R.id.tvRowSubtitle);
                ImageView ivRow = (ImageView)convertView.findViewById(R.id.imgRow);

                Album album = this.getItem(position);
                new ImageLoaderTask(ivRow, album.Image).execute();
                tvTitle.setText(album.Name);
                tvSubtitle.setText(album.Year.toString());

                return convertView;
            }
        };

        ListView lv = (ListView)this._view.findViewById(R.id.lvAlbums);
        lv.setAdapter(this._adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Album album = _adapter.getItem(position);
                _callback.AlbumSelected(album.Id);
            }
        });
    }

    /**
     * Método que iniciará un diálogo de carga en la interfaz.
     * @param message
     */
    private void StartProgressDialog(String message){
        this._progressDialog = ProgressDialog.show(getActivity(), "Cargando...", message);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try{
            this._callback = (IArtistFragmentCallback)activity;
        } catch (ClassCastException excep){
            Log.d("Log", "The activity class must implement IArtistFragmentCallback.");
            throw excep;
        }
    }

    /**
     * Método que ocultará el diálogo de carga de la interfaz.
     */
    private void DismissProgressDialog(){
        if (this._progressDialog != null)
            this._progressDialog.dismiss();
    }

    private void ShowError(String error){
        this.DismissProgressDialog();
        Log.d("error", error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Error");
        builder.setMessage("Se ha producido un error cargando los datos");
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    @Override
    public void AlbumsReceived(ArrayList<Album> albums) {
        this.DismissProgressDialog();
        this._adapter.clear();
        this._adapter.addAll(albums);
    }

    @Override
    public void ErrorLoading(String error) {
        this.ShowError(error);
    }

    @Override
    public void ArtistReceived(Artist artist) {
        this.DismissProgressDialog();
        TextView tvArtistName = (TextView)this._view.findViewById(R.id.txtArtistName);
        ImageView ivArtist = (ImageView)this._view.findViewById(R.id.ivArtist);

        tvArtistName.setText(artist.Name);
        new ImageLoaderTask(ivArtist, artist.Image)
                .execute();
    }

    @Override
    public void Error(String error) {
        this.ShowError(error);
    }

    public interface IArtistFragmentCallback{
        void AlbumSelected(Integer id);
    }
}
