package com.contoso.hellocasette;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.contoso.hellocasette.businessobjects.IUserLoginBO;
import com.contoso.hellocasette.businessobjects.IUserLoginBO.IUserLoginBOCallback;
import com.contoso.hellocasette.businessobjects.UserLoginBO;

public class LoginActivity extends Activity 
		implements IUserLoginBOCallback{

	IUserLoginBO _businessObjects;
    ProgressDialog _progressDialog;
	
	/**
	 * Métodos del ciclo de vida de la actividad.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

        //Inicialización de los objetos de negocio.
		this._businessObjects = new UserLoginBO(this, this);

        //Comprobamos si el usuario ha iniciado la sesión previamente.
        //Si la ha iniciado, lo reenviaremos a la pantalla principal
        //del sistema.
        if (this._businessObjects.IsUserLoggedIn())
            StartMainActivity();
	}
	
	/**
	 * Métodos generales.
	 */

    /*
    El método StartMainActivity, permitirá iniciar la actividad
    principal desde otros métodos de la actividad
     */
	public void StartMainActivity(){
		Intent intent =
				new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}
	
	/**
	 * Eventos de la interfaz de usuario
	 */
	public void btnLoginClick(View v){
        //Llamaremos a los objetos de negocio con los valores de usuario y contraseña
		EditText etUsername = (EditText)this.findViewById(R.id.txtusername);
		EditText etPassword = (EditText)this.findViewById(R.id.txtpassword);

        this.StartProgressDialog("Validando usuario");
		this._businessObjects.Validate(
				etUsername.getText().toString(),
				etPassword.getText().toString());
	}

    /**
     * Evento de registro del usuario.
     * Iniciará la actividad de registro.
     * @param v
     */
	public void btnRegisterClick(View v){
		Intent intentRegister =
				new Intent(this, RegisterActivity.class);
		startActivity(intentRegister);
	}

    /**
     * Método que iniciará un diálogo de carga en la interfaz.
     * @param message
     */
    private void StartProgressDialog(String message){
        this._progressDialog = ProgressDialog.show(this, "Cargando...", message);
    }

    /**
     * Método que ocultará el diálogo de carga de la interfaz.
     */
    private void DismissProgressDialog(){
        if (this._progressDialog != null)
            this._progressDialog.dismiss();
    }

	/**
	 * Métodos del callback de IUserLoginBO.
	 */

    /**
     * Método del callback IUserLoginBOCallback, que se ejecutará
     * cuando el servicio haya respondido con el inicio de sesión
     * del usuario.
     * @param result
     */
	@Override
	public void LoggedIn(Boolean result) {
        this.DismissProgressDialog();
		if (result)
			this.StartMainActivity();
        else
            this.Error("No se ha podido iniciar la sesión del usuario");
	}

    /**
     * Método del callback IUserLoginBOCallback que se ejecutará si existe
     * un error en la carga de los datos o el inicio de sesión
     * @param error
     */
	@Override
	public void Error(String error) {
        this.DismissProgressDialog();
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
		alertBuilder.setTitle("Error");
		alertBuilder.setMessage(error);
		alertBuilder.setPositiveButton("OK", null);
		
		alertBuilder.show();
	}
}
