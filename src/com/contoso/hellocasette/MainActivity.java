package com.contoso.hellocasette;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.contoso.hellocasette.MusicFragment.IMusicFragmentCallback;
import com.contoso.hellocasette.businessobjects.BusinessObjectsBase;
import com.contoso.hellocasette.crosscutting.Song;
import com.contoso.hellocasette.crosscutting.services.MusicService;

import java.util.ArrayList;

public class MainActivity extends Activity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks,
		IMusicFragmentCallback, ArtistFragment.IArtistFragmentCallback,
        AlbumFragment.IAlbumFragmentCallback, MusicService.IMusicServiceCallback{
	
	MainFragmentManager _mainFragmentManager;
    MusicService _musicService;
	
	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Estableceremos las áreas que se van a mostrar en el
     * fragmento lateral con las áreas del programa.
     */
	public static final String[] Sections =
			new String[]{
				"Música",
				"Playlists",
				"Favoritos",
				"Mi cuenta",
                "Usuarios",
				"Desconectar"
			};

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        this._mainFragmentManager = new MainFragmentManager();

		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();
        this._musicService = new MusicService(getApplicationContext(), this);

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                shouldDisplayHomeUp();
            }
        });
	}

    /**
     * Este evento se ejecutará cuando el usuario pulse
     * uno de los items del lateral de la ventana.
     * @param position
     */
	@Override
	public void onNavigationDrawerItemSelected(int position) {
        if (position == 5){
            Logout();
        } else {
            OpenFragment(position, null);
        }
	}

    public void shouldDisplayHomeUp(){
        //Enable Up button only  if there are entries in the back stack
        boolean canback = getFragmentManager().getBackStackEntryCount()>0;
        getActionBar().setDisplayHomeAsUpEnabled(canback);
    }

    private void OpenFragment(int position, Bundle args){
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = this._mainFragmentManager.GetFragmentByPosition(position);

        if (fragment != null) {
            if (args != null)
                fragment.setArguments(args);

            FragmentTransaction transaction = fragmentManager
                    .beginTransaction();

            //Si el fragmento necesita vuelta atrás, lo agregamos al BackStack
            if (this._mainFragmentManager.AddToBackStack(position))
                transaction.addToBackStack(null);
            else
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            transaction.replace(R.id.container, fragment)
                    .commit();
        }
    }

    /**
     * Evento que ejecutaremos cuando el usuario pulse sobre desconexión.
     * Al pulsar sobre Sí, lo mandaremos a la ventana del login.
     */
    private void Logout(){

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Atención");
        dialog.setMessage("¿Estás seguro de cerrar la sesión?");
        dialog.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                _musicService.Stop();
                GoToLogin();
            }
        });
        dialog.setNegativeButton("No", null);
        dialog.show();
    }

    /**
     * Evento que enviará el flujo del sistema al login, y limpiará el backstack
     * para que no se pueda volver a esta pantalla pulsando el botón atrás del teléfono.
     */
    private void GoToLogin(){
        BusinessObjectsBase.ResetToken(this);
        Intent intent = new Intent(this, LoginActivity.class);
        //Con esta línea indicaremos que vamos a limpiar el backstack de actividades del teléfono.
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Evento que se ejecutará cuando se pulse un elemento
     * de artista del fragmento de Música
	 */
	@Override
	public void OnArtistSelected(int id) {
		Bundle args = new Bundle();
        args.putInt("id", id);

        this.OpenFragment(MainFragmentIdentifier.ARTIST.GetValue(), args);
	}

    @Override
    public void AlbumSelected(Integer id) {
        Bundle args = new Bundle();
        args.putInt("id", id);

        this.OpenFragment(MainFragmentIdentifier.ALBUM.GetValue(), args);
    }

    LinearLayout _llSong;
    Button _btnPlayPause;

    private Button GetButtonPlayPause(){
        if (this._btnPlayPause == null)
            this._btnPlayPause = (Button)this.findViewById(R.id.btnPlayPause);

        return this._btnPlayPause;
    }

    @Override
    public void PlaySongs(ArrayList<Song> songs) {
        this._musicService.SetSongs(songs);
    }

    @Override
    public void PlayingSong(Song data) {
        if (this._llSong == null)
            this._llSong = (LinearLayout)this.findViewById(R.id.llSongPlayer);

        this._llSong.setVisibility(View.VISIBLE);
        TextView tvSongTitle = (TextView)this.findViewById(R.id.txtSong);
        tvSongTitle.setText(data.Name);

        TextView tvSongSubtitle = (TextView)this.findViewById(R.id.txtSubsong);
        tvSongSubtitle.setText(data.Quality);
    }

    @Override
    public void Stopped() {
        this.GetButtonPlayPause().setText("Play");
    }

    @Override
    public void Paused() {
        this.GetButtonPlayPause().setText("Play");
    }

    @Override
    public void Played() {
        this.GetButtonPlayPause().setText("Pause");
    }

    @Override
    public void ErrorPlayingSong(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void btnPlayPauseClicked(View v){
        this._musicService.PlayPause();
    }

    /**
     * Clase creada para gestionar los fragmentos.
     * Gestionará que no se cree más de una instancia del mismo fragmento
     * y que dependiendo de la posición devuelve el elemento Fragment
     * correspondiente.
     */
	private class MainFragmentManager{
		
		private MusicFragment _musicFragment;

        /**
         * Obtención del fragmento por posición
         * @param position Posición del fragmento que se debe iniciar
         * @return Fragmento creado
         */
		public Fragment GetFragmentByPosition(int position){
			Fragment fragment = null;
			
			if (position == MainFragmentIdentifier.MUSIC.GetValue()){
				if (this._musicFragment == null)
					this._musicFragment = new MusicFragment();
				
				fragment = this._musicFragment;
			} else if (position == MainFragmentIdentifier.ARTIST.GetValue()){
                fragment = new ArtistFragment();
            } else if (position == MainFragmentIdentifier.ALBUM.GetValue()){
                fragment = new AlbumFragment();
            }

            return fragment;
		}

        /**
         * Este método obtendrá, a partir de una posición, el título del fragmento.
         * @param position
         * @return
         */
        public String GetTitle(int position){
            return MainFragmentIdentifier.GetIdentifierForPosition(position).GetTitle();
        }

        /**
         * Este método determinará, a partir de una posición, si el fragmento debe agregarse al backstack
         * @param position
         * @return Booleano indicando si debe agregarse al callstack
         */
        public Boolean AddToBackStack(int position){
            Boolean result = false;

            if (MainFragmentIdentifier.GetIdentifierForPosition(position) ==
                    MainFragmentIdentifier.ARTIST)
                result = true;
            else if (MainFragmentIdentifier.GetIdentifierForPosition(position) ==
                    MainFragmentIdentifier.ALBUM)
                result = true;

            return result;
        }
		
	}

    /**
     * Enumeración para controlar la posición y título de los fragmentos
     * que se van a poder abrir desde la actividad
     */
    private enum MainFragmentIdentifier{
        MUSIC(0, "Música"), PLAYLISTS(1, "Playlists"), FAVORITES(2, "Favoritos"),
        ACCOUNT(3, "Cuenta"), USERS(4, "Usuarios"), LOGOUT(5, "Desconectar"), ARTIST(6, "Artista"),
        ALBUM(7, "Álbum");

        Integer _id;
        String _title;
        MainFragmentIdentifier(Integer id, String title){
            this._id = id;
            this._title = title;
        }

        public Integer GetValue() {
            return _id;
        }

        public String GetTitle(){
            return this._title;
        }

        public static MainFragmentIdentifier GetIdentifierForPosition(Integer id){
            MainFragmentIdentifier ident = MainFragmentIdentifier.MUSIC;

            if (id == MainFragmentIdentifier.PLAYLISTS.GetValue()){
                ident = MainFragmentIdentifier.PLAYLISTS;
            } else if (id == MainFragmentIdentifier.FAVORITES.GetValue()){
                ident = MainFragmentIdentifier.FAVORITES;
            } else if (id == MainFragmentIdentifier.ACCOUNT.GetValue()){
                ident = MainFragmentIdentifier.ACCOUNT;
            } else if (id == MainFragmentIdentifier.USERS.GetValue()){
                ident = MainFragmentIdentifier.USERS;
            } else if (id == MainFragmentIdentifier.ARTIST.GetValue()){
                ident = MainFragmentIdentifier.ARTIST;
            } else if (id == MainFragmentIdentifier.ALBUM.GetValue()){
                ident = MainFragmentIdentifier.ALBUM;
            }

            return ident;
        }
    }
}
