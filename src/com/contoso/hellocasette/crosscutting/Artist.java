package com.contoso.hellocasette.crosscutting;

public class Artist {
	public Integer Id;
	public String Name;
	public String Biography;
	public String Image;
	public Integer Year;
	
	public Artist(){
	}
	
	public Artist(
			Integer id,
			String name,
			String biography,
			String image,
			Integer year){
		this.Id = id;
		this.Name = name;
		this.Biography = biography;
		this.Image = image;
		this.Year = year;
	}
}
