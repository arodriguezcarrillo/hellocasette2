package com.contoso.hellocasette.crosscutting.services;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import com.contoso.hellocasette.crosscutting.Song;

import java.util.ArrayList;

/**
 * Created by alberto on 8/2/15.
 */
public class MusicService {
    ArrayList<Song> _songs;
    Context _context;
    Boolean _repeat = false;
    Integer _playingSong = 0;
    Boolean _playing = false;
    MediaPlayer _mediaPlayer;
    IMusicServiceCallback _callback;
    Boolean _setted = false;

    public MusicService(Context context, IMusicServiceCallback callback){
        this._context = context;
        this._callback = callback;
        this._mediaPlayer = new MediaPlayer();
        this._mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                GoToNext();
            }
        });

        this._mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                if (_callback != null)
                    _callback.ErrorPlayingSong("La canción no ha podido cargarse");

                return true;
            }
        });
    }

    public void SetSongs(ArrayList<Song> songs){
        this._songs = songs;
        if (this._songs.size() > 0) {
            this.Play(0);
        }
    }

    public void PlayPause(){
        if (this._playing)
            this.Pause();
        else
            this.Play();
    }

    public void Pause(){
        if (this._mediaPlayer.isPlaying()) {
            this._mediaPlayer.pause();
            this._playing = false;

            if (this._callback != null)
                this._callback.Paused();
        }
    }

    public void Play(){
        if (!this._mediaPlayer.isPlaying()) {
            this._mediaPlayer.start();
            this._playing = true;

            if (this._callback != null)
                this._callback.Played();
        }
    }

    private void Play(Integer position){
        if (this._songs.size() > position){
            try {
                this._playingSong = position;
                Song song = this._songs.get(position);

                if (this._mediaPlayer.isPlaying()) {
                    this._mediaPlayer.stop();
                }

                if (this._setted)
                    this._mediaPlayer.reset();

                this._setted = true;
                this._mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                this._mediaPlayer.setDataSource(this._context, Uri.parse(song.Uri));

                if (this._callback != null)
                    this._callback.PlayingSong(song);

                this._mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                        _playing = true;
                        if (_callback != null)
                            _callback.Played();
                    }
                });

                this._mediaPlayer.prepareAsync();
            } catch (Exception excep){
                Log.d("error", excep.getMessage());
                if (this._callback != null)
                    this._callback.ErrorPlayingSong("Se ha producido un error cargando la canción");
                this.GoToNext();
            }
        }
    }

    private void GoToNext(){
        if ((this._playingSong + 1) < this._songs.size()){
            this.Play(this._playingSong + 1);
        } else if (this._repeat) {
            this.Play(0);
        } else {
            this.Stop();
        }
    }

    public void Stop(){
        if (this._mediaPlayer.isPlaying()) {
            this._mediaPlayer.stop();
            this._playing = false;

            if (this._callback != null)
                this._callback.Stopped();
        }
    }

    public interface IMusicServiceCallback{
        void PlayingSong(Song data);
        void Stopped();
        void Paused();
        void Played();
        void ErrorPlayingSong(String message);
    }
}
