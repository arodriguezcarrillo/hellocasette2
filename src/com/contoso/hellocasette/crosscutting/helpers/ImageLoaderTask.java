package com.contoso.hellocasette.crosscutting.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.net.URL;

/**
 * Clase para realizar la carga de una imagen
 * sobre un ImageView.
 */
public class ImageLoaderTask extends AsyncTask<Void, Void, Bitmap> {
    ImageView _iv;
    String _url;

    public ImageLoaderTask(ImageView iv, String url){
        this._iv = iv;
        this._url = url;
    }


    @Override
    protected Bitmap doInBackground(Void... params) {
        Bitmap res = null;
        try{
            URL url = new URL(this._url);
            res = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception excep){
            Log.d("error", excep.getMessage());
        }

        return res;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        this._iv.setImageBitmap(result);
    }
}
