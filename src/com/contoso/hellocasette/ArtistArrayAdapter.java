package com.contoso.hellocasette;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.contoso.hellocasette.crosscutting.Artist;
import com.contoso.hellocasette.crosscutting.helpers.ImageLoaderTask;

import java.util.List;

public class ArtistArrayAdapter extends ArrayAdapter<Artist> {
	LayoutInflater _li;
	List<Artist> _items;
	int _resource;
	
	public ArtistArrayAdapter(Context context, int resource,
			List<Artist> objects) {
		super(context, resource, objects);
		
		this._li = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this._items = objects;
		this._resource = resource;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null){
			convertView = this._li.inflate(R.layout.grid_item, null);
		}
		
		Artist item = this._items.get(position);
		TextView tvTitle = (TextView)convertView.findViewById(R.id.tvRowTitle);
		TextView tvSubtitle = (TextView)convertView.findViewById(R.id.tvRowSubtitle);
        ImageView ivRow = (ImageView)convertView.findViewById(R.id.imgRow);
		
		tvTitle.setText(item.Name);
		tvSubtitle.setText(item.Year.toString());

        new ImageLoaderTask(ivRow, item.Image).execute();
		
		return convertView;
	}
	
}
