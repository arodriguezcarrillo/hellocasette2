package com.contoso.hellocasette;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.contoso.hellocasette.businessobjects.AlbumsBO;
import com.contoso.hellocasette.businessobjects.IAlbumsBO;
import com.contoso.hellocasette.businessobjects.ISongsBO;
import com.contoso.hellocasette.businessobjects.SongsBO;
import com.contoso.hellocasette.crosscutting.Album;
import com.contoso.hellocasette.crosscutting.Song;
import com.contoso.hellocasette.crosscutting.helpers.ImageLoaderTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alberto on 8/2/15.
 */
public class AlbumFragment extends Fragment
        implements ISongsBO.ISongsBOCallback, IAlbumsBO.IAlbumBOCallback {

        ProgressDialog _progressDialog;
        SongsBO _businessObjects;
        AlbumsBO _albumBusinessObjects;
        ArrayAdapter<Song> _adapter;
        ArrayList<Song> _songs;
        IAlbumFragmentCallback _callback;
        Integer _albumId;
        View _view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            setHasOptionsMenu(true);
            this._view = inflater.inflate(R.layout.fragment_album, null);

            this.InitializeInterfaceActions();
            this.InitializeBusinessObjects();
            Bundle bundle = getArguments();
            this._albumId = bundle.getInt("id");
            this.LoadInitialData();

            return this._view;
        }

    private void InitializeBusinessObjects() {
        this._businessObjects = new SongsBO(getActivity(), this);
        this._albumBusinessObjects = new AlbumsBO(getActivity(), this);
    }

    private void LoadInitialData(){
            this.StartProgressDialog("Cargando álbumes y datos del artista");
            this._businessObjects.Songs(this._albumId);
            this._albumBusinessObjects.Album(this._albumId);
        }

    private void InitializeInterfaceActions(){
        Button btnPlayAll = (Button)this._view.findViewById(R.id.btnPlayAll);
        btnPlayAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_callback != null)
                    _callback.PlaySongs(_songs);
            }
        });

        this._adapter = new ArrayAdapter<Song>(getActivity(),
            android.R.layout.simple_list_item_2, new ArrayList<Song>()){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                    if (convertView == null)
                    convertView = getActivity().getLayoutInflater().inflate(android.R.layout.simple_list_item_2,
                    null);

                TextView tvTitle = (TextView)convertView.findViewById(android.R.id.text1);
                TextView tvSubtitle = (TextView)convertView.findViewById(android.R.id.text2);

                Song song = this.getItem(position);
                tvTitle.setText(song.Name);
                tvSubtitle.setText(song.Quality);

                return convertView;
            }
        };

        ListView lv = (ListView)this._view.findViewById(R.id.lvSongs);
        lv.setAdapter(this._adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Song song = _adapter.getItem(position);

                ArrayList<Song> songs = new ArrayList<Song>();
                songs.add(song);
                if (_callback != null)
                    _callback.PlaySongs(songs);
            }
        });
    }

    /**
     * Método que iniciará un diálogo de carga en la interfaz.
     * @param message
     */
    private void StartProgressDialog(String message){
            this._progressDialog = ProgressDialog.show(getActivity(), "Cargando...", message);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try{
            this._callback = (IAlbumFragmentCallback)activity;
        } catch (ClassCastException excep){
            Log.d("Log", "The activity class must implement IArtistFragmentCallback.");
            throw excep;
        }
    }

    /**
     * Método que ocultará el diálogo de carga de la interfaz.
     */
    private void DismissProgressDialog(){
        if (this._progressDialog != null)
        this._progressDialog.dismiss();
    }

    private void ShowError(String error){
        this.DismissProgressDialog();
        Log.d("error", error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Error");
        builder.setMessage("Se ha producido un error cargando los datos");
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    @Override
    public void SongsReceived(ArrayList<Song> songs) {
        this.DismissProgressDialog();
        this._songs = songs;
        this._adapter.clear();
        this._adapter.addAll(songs);
    }

    @Override
    public void Error(String error) {
        this.ShowError(error);
    }

    @Override
    public void AlbumReceived(Album album) {
        this.DismissProgressDialog();
        TextView tvAlbumName = (TextView)this._view.findViewById(R.id.txtAlbumName);
        ImageView ivAlbum = (ImageView)this._view.findViewById(R.id.ivAlbum);

        tvAlbumName.setText(album.Name);
        new ImageLoaderTask(ivAlbum, album.Image)
                .execute();
    }

    @Override
    public void ErrorLoading(String error) {
        this.ShowError(error);
    }

    public interface IAlbumFragmentCallback{
        void PlaySongs(ArrayList<Song> songs);
    }
}
