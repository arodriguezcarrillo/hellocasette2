package com.contoso.hellocasette;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.contoso.hellocasette.businessobjects.IUserRegisterBO;
import com.contoso.hellocasette.businessobjects.IUserRegisterBO.IUserRegisterBOCallback;
import com.contoso.hellocasette.businessobjects.UserRegisterBO;

public class RegisterActivity extends Activity
		implements IUserRegisterBOCallback{
	IUserRegisterBO _businessObjects;
	ProgressDialog _progressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        //Inicialización de los objetos de negocio.
		this._businessObjects = new UserRegisterBO(this, this);
		
		setContentView(R.layout.activity_register);
		
		getActionBar().hide();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

    /**
     * Método que se ejecutará cuando el usuario pulse el botón
     * Back de la interfaz.
     * @param v
     */
	public void btnBackClick(View v){
		this.finish();
	}
	
	public void btnRegisterClick(View v){
		EditText etName = (EditText)findViewById(R.id.txtname);
		EditText etSurname = (EditText)findViewById(R.id.txtsurname);
		EditText etEmail = (EditText)findViewById(R.id.txtemail);
		EditText etRemail = (EditText)findViewById(R.id.txtremail);
		EditText etPassword = (EditText)findViewById(R.id.txtpassword);
		EditText etRepassword = (EditText)findViewById(R.id.txtrepassword);
		EditText etAge = (EditText)findViewById(R.id.txtage);
		
		this.StartProgressDialog("Realizando registro de usuario");

        //Llamamos al evento del registro.
		this._businessObjects.Register(etName.getText().toString(), 
				etSurname.getText().toString(), 
				etPassword.getText().toString(), 
				etRepassword.getText().toString(), 
				etEmail.getText().toString(), 
				etRemail.getText().toString(), 
				etAge.getText().toString());
	}

    /**
     * Método de inicialización de diálogo de carga
     * @param message
     */
	private void StartProgressDialog(String message){
		this._progressDialog = ProgressDialog.show(this, "Cargando...", message);
	}

    /**
     * Método de ocultación del diálogo de carga.
     */
	private void DismissProgressDialog(){
		if (this._progressDialog != null)
			this._progressDialog.dismiss();
	}

    /**
     * Evento del callback de los objetos de negocio
     * indicando que el resultado del registro ha sido
     * exitoso. Lo reenviaremos a la actividad Main.
     */
	@Override
	public void Registered() {
		this.DismissProgressDialog();
		Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}

    /**
     * Evento del callback de los objetos de negocio
     * que se ejecutará cuando exista algún error en
     * el registro del usuario.
     * @param error
     */
	@Override
	public void ErrorOnRegistration(String error) {
		this.DismissProgressDialog();
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Error en el registro");
		dialog.setMessage(error);
		dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		
		dialog.show();
	}
}
