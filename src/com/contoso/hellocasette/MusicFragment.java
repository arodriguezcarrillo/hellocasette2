package com.contoso.hellocasette;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.contoso.hellocasette.businessobjects.IArtistsBO;
import com.contoso.hellocasette.businessobjects.ArtistsBO;
import com.contoso.hellocasette.crosscutting.Artist;

import java.util.ArrayList;

public class MusicFragment extends Fragment implements IArtistsBO.IArtistsBOCallback{
	IMusicFragmentCallback _callback;
    ArtistArrayAdapter _adapter;
    IArtistsBO _businessObjects;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        if (this._businessObjects == null)
            this._businessObjects = new ArtistsBO(getActivity(), this);

        setHasOptionsMenu(true);
		View v = inflater.inflate(R.layout.fragment_music, null);
		
		this._adapter = new ArtistArrayAdapter(getActivity(),R.layout.grid_item,new ArrayList<Artist>());
        ListView lv = (ListView)v.findViewById(R.id.lvMusic);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Artist item = _adapter.getItem(position);
                _callback.OnArtistSelected(item.Id);
            }
        });
        lv.setAdapter(this._adapter);
        this._businessObjects.Artists("");
		
		return v;
	}

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        ActionBar actionBar = getActionBar();
        inflater.inflate(R.menu.artists, menu);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle("Música");
    }

    private ActionBar getActionBar() {
        return getActivity().getActionBar();
    }

    @Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try{
			this._callback = (IMusicFragmentCallback)activity;
		} catch (ClassCastException excep){
			Log.d("Log", "The activity class must implement IMusicFragmentCallback.");
			throw excep;
		}
	}

    @Override
    public void ArtistsReceived(ArrayList<Artist> artists) {
        this._adapter.clear();
        this._adapter.addAll(artists);
    }

    @Override
    public void Error(String error) {
        Log.d("error", error);
    }


    public interface IMusicFragmentCallback{
		void OnArtistSelected(int id);
	}
}
