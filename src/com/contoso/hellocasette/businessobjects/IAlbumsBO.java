package com.contoso.hellocasette.businessobjects;

import com.contoso.hellocasette.crosscutting.Album;

import java.util.ArrayList;

/**
 * Interfaz para definir la obtención de álbums desde el servicio
 */
public interface IAlbumsBO {

    void Albums();
    void Albums(String search);
    void Albums(Integer artist);
    void Album(Integer id);

    public interface IAlbumsBOCallback{
        void AlbumsReceived(ArrayList<Album> albums);
        void ErrorLoading(String error);
    }

    public interface IAlbumBOCallback{
        void AlbumReceived(Album album);
        void ErrorLoading(String error);
    }
}
