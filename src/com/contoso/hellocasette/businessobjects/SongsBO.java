package com.contoso.hellocasette.businessobjects;

import android.content.Context;

import com.contoso.hellocasette.crosscutting.Song;
import com.contoso.hellocasette.dal.CallType;
import com.contoso.hellocasette.dal.RestCallTask;
import com.contoso.hellocasette.dal.RestParameter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by alberto on 8/2/15.
 */
public class SongsBO extends BusinessObjectsBase
        implements ISongsBO, RestCallTask.IRestCallTaskCallback {
    ISongsBOCallback _callback;

    public SongsBO(Context context, ISongsBOCallback callback){
        super(context);
        this._callback = callback;
    }

    @Override
    public void Songs(Integer album) {
        ArrayList<RestParameter> params = new ArrayList<RestParameter>();

        params.add(new RestParameter("token", this.GetToken()));
        params.add(new RestParameter("album", album.toString()));

        RestParameter[] data = new RestParameter[params.size()];
        data = params.toArray(data);

        //Creación de la tarea REST para invocar al servicio.
        new RestCallTask(SongsBOCallType.SONGS.GetValue(),"songs/all.json", this, CallType.GET)
                .execute(data);
    }

    @Override
    public void Executed(Integer id, String result, Boolean error) {
        if (id == SongsBOCallType.SONGS.GetValue()){
            if (!error){
                ProcessSongsResponse(result);
            } else {
                this.RaiseError("Se ha producido un error cargando los artistas");
            }
        }
    }

    private void ProcessSongsResponse(String response){
        try{
            ArrayList<Song> result = new ArrayList<Song>();

            JSONArray artists = new JSONObject(response).getJSONArray("data");

            Integer index = 0;
            while (index < artists.length()){
                JSONObject artist = artists.getJSONObject(index);

                Song itemToAdd = new Song();
                itemToAdd.Id = artist.getInt("id");
                itemToAdd.Name = artist.getString("name");
                itemToAdd.Number = artist.getInt("number");
                itemToAdd.Duration = artist.getInt("duration");
                itemToAdd.Quality = artist.getString("quality");
                itemToAdd.Uri = artist.getString("uri");
                result.add(itemToAdd);

                index++;
            }

            if (this._callback != null)
                this._callback.SongsReceived(result);

        } catch (Exception excep){
            this.RaiseError("Se ha producido un error cargando los artistas");
        }
    }

    private void RaiseError(String error){
        if (this._callback != null)
            this._callback.Error(error);
    }

    public enum SongsBOCallType{
        SONGS(1);

        Integer _id;
        SongsBOCallType(Integer id){
            this._id = id;
        }

        public Integer GetValue() {
            return _id;
        }
    }
}
