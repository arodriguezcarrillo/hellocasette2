package com.contoso.hellocasette.businessobjects;

public interface IUserLoginBO {
	void Validate(String user, String password);
	Boolean IsUserLoggedIn();
	
	public interface IUserLoginBOCallback{
		void LoggedIn(Boolean result);
		void Error(String error);
	}
}
