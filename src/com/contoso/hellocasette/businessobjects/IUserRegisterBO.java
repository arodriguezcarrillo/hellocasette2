package com.contoso.hellocasette.businessobjects;

public interface IUserRegisterBO {
	void Register(
		String name,
		String surname,
		String password,
		String repassword,
		String email,
		String remail,
		String age
			);
	
	public interface IUserRegisterBOCallback{
		void Registered();
		void ErrorOnRegistration(String error);
	}
}
