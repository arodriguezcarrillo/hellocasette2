package com.contoso.hellocasette.businessobjects;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Con esta clase, que heredarán todas las clases de objetos de negocio
 * habilitaremos la capacidad de leer y escribir el token en cualquier momento.
 */
public abstract class BusinessObjectsBase {
	final static String TOKEN_IDENTIFIER = "token";
	Context  _context;
	
	public BusinessObjectsBase(Context context){
		this._context = context;
	}

    /**
     * Método de guardado del token dentro de las preferencias compartidas.
     * @param token
     */
	protected void SaveToken(String token){
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this._context);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(TOKEN_IDENTIFIER, token);
		editor.commit();
	}

    /**
     * Obtención del token desde las preferencias compartidas.
     * @return
     */
	protected String GetToken(){
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this._context);
		String result = "";
		
		if (sharedPref.contains(TOKEN_IDENTIFIER))
			result = sharedPref.getString(TOKEN_IDENTIFIER, "");
		
		return result;
	}

    public static void ResetToken(Context context){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(TOKEN_IDENTIFIER);
        editor.commit();
    }
	
}
