package com.contoso.hellocasette.businessobjects;

import com.contoso.hellocasette.crosscutting.Artist;

import java.util.ArrayList;

/**
 * Interfaz de objetos de negocio de ArtistsBO para utilizar en fragmentos.
 */
public interface IArtistsBO {
    void Artists(String search);
    void Artist(Integer artist);

    public interface IArtistsBOCallback{
        void ArtistsReceived(ArrayList<Artist> artists);
        void Error(String error);
    }

    public interface IArtistBOCallback{
        void ArtistReceived(Artist artist);
        void Error(String error);
    }
}
