package com.contoso.hellocasette.businessobjects;

import android.content.Context;

import com.contoso.hellocasette.crosscutting.Artist;
import com.contoso.hellocasette.dal.CallType;
import com.contoso.hellocasette.dal.RestCallTask;
import com.contoso.hellocasette.dal.RestParameter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Clase que implementa la interfaz IArtistsBO. Necesitará un callback
 * de tipo IMusicBOCallback para comunicarse ascedendentemente.
 */
public class ArtistsBO extends BusinessObjectsBase
        implements IArtistsBO, RestCallTask.IRestCallTaskCallback {

    IArtistsBOCallback _callback;
    IArtistBOCallback _artistCallback;

    public ArtistsBO(Context context, IArtistsBOCallback callback){
        super(context);
        this._callback = callback;
    }

    public ArtistsBO(Context context, IArtistBOCallback callback){
        super(context);
        this._artistCallback = callback;
    }

    /**
     * Método que indica que se van a querer obtener los
     * artistas desde el servicio REST.
     * @param search
     */
    @Override
    public void Artists(String search) {

        ArrayList<RestParameter> params = new ArrayList<RestParameter>();

        params.add(new RestParameter("token", this.GetToken()));
        if (!search.isEmpty()) {
            params.add(new RestParameter("search", search));
        }

        RestParameter[] data = new RestParameter[params.size()];
        data = params.toArray(data);

        //Creación de la tarea REST para invocar al servicio.
        new RestCallTask(ArtistsBOCallType.ARTISTS.GetValue(),"artists/all.json", this, CallType.GET)
                .execute(data);
    }

    @Override
    public void Artist(Integer artist) {

        ArrayList<RestParameter> params = new ArrayList<RestParameter>();

        params.add(new RestParameter("token", this.GetToken()));
        params.add(new RestParameter("id", artist.toString()));

        RestParameter[] data = new RestParameter[params.size()];
        data = params.toArray(data);

        //Creación de la tarea REST para invocar al servicio.
        new RestCallTask(ArtistsBOCallType.ARTIST.GetValue(),
                "artists/item.json",
                this,
                CallType.GET)
                .execute(data);
    }

    /**
     * Evento para propagar un error hacia la interfaz desde cualquier
     * punto de la clase
     * @param error
     */
    private void RaiseError(String error){
        if (this._callback != null)
            this._callback.Error(error);
    }

    /**
     * Método del callabck que se ejecutará cuando la llamada al servicio
     * REST se haya completado.
     * @param id Identificador de la llamada que habremos pasado previamente.
     * @param result Resultado string de la llamada.
     * @param error Booleano indicando si ha existido un error en la llamada
     */
    @Override
    public void Executed(Integer id, String result, Boolean error) {
        if (id == ArtistsBOCallType.ARTISTS.GetValue()){
            if (!error){
                ProcessArtistsResponse(result);
            } else {
                this.RaiseError("Se ha producido un error cargando los artistas");
            }
        }else if (id == ArtistsBOCallType.ARTIST.GetValue()){
            if (!error){
                ProcessArtistResponse(result);
            } else {
                if (this._artistCallback != null)
                    this._artistCallback.Error(result);
            }
        }
    }

    private void ProcessArtistResponse(String response){
        try{
            JSONObject artist = new JSONObject(response).getJSONObject("data");

            Artist result = new Artist();
            result.Id = artist.getInt("id");
            result.Name = artist.getString("name");
            result.Year = artist.getInt("year");
            result.Image = artist.getString("image");

            if (this._artistCallback != null)
                this._artistCallback.ArtistReceived(result);

        } catch (Exception excep){
            if (this._artistCallback != null)
                this._artistCallback.Error("Se ha producido un error cargando los datos del artista");
        }
    }

    /**
     * Método para procesar la respuesta del método de obtención
     * de artistas.
     * @param response
     */
    private void ProcessArtistsResponse(String response){
        try{
            ArrayList<Artist> result = new ArrayList<Artist>();

            JSONArray artists = new JSONObject(response).getJSONArray("data");

            Integer index = 0;
            while (index < artists.length()){
                JSONObject artist = artists.getJSONObject(index);

                Artist itemToAdd = new Artist();
                itemToAdd.Id = artist.getInt("id");
                itemToAdd.Name = artist.getString("name");
                itemToAdd.Year = artist.getInt("year");
                itemToAdd.Image = artist.getString("image");
                result.add(itemToAdd);

                index++;
            }

            if (this._callback != null)
                this._callback.ArtistsReceived(result);

        } catch (Exception excep){
            this.RaiseError("Se ha producido un error cargando los artistas");
        }
    }

    /**
     * Enumeración creada para clasificar las llamadas que se van a realizar
     * a las tareas de tipo REST. Al contestar todas las llamadas de RestCallTask
     * dentro del mismo método, necesitaremos clasificar a través de un identificador
     * el tipo de llamada que se ha realizado.
     */
    private enum ArtistsBOCallType{
        ARTISTS(1),ARTIST(2);

        Integer _id;
        ArtistsBOCallType(Integer id){
            this._id = id;
        }

        public Integer GetValue() {
            return _id;
        }
    }
}
