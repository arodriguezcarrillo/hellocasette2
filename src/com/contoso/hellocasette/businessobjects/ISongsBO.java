package com.contoso.hellocasette.businessobjects;

import com.contoso.hellocasette.crosscutting.Song;

import java.util.ArrayList;


public interface ISongsBO {
    void Songs(Integer album);

    public interface ISongsBOCallback{
        void SongsReceived(ArrayList<Song> songs);
        void Error(String error);
    }
}
