package com.contoso.hellocasette.businessobjects;

import android.content.Context;

import com.contoso.hellocasette.dal.CallType;
import com.contoso.hellocasette.dal.RestCallTask;
import com.contoso.hellocasette.dal.RestCallTask.IRestCallTaskCallback;
import com.contoso.hellocasette.dal.RestParameter;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Clase de objetos de negocio para el login del
 * usuario. Necesitará un objeto de tipo IUserLoginBOCallback
 * para poder propagar sus acciones hacia arriba.
 */
public class UserLoginBO extends BusinessObjectsBase
			implements IUserLoginBO, IRestCallTaskCallback {

	IUserLoginBOCallback _callback;
	
	public UserLoginBO(Context context, IUserLoginBOCallback callback){
		super(context);
		this._callback = callback;
	}
	
	/**
	 * Método que podremos llamar para realizar la validación
     * de usuario y contraseña contra el servicio REST.
	 */
	@Override
	public void Validate(String user, String password) {
		RestCallTask task = 
				new RestCallTask(
                    UserLoginBOCallType.VALIDATE.GetValue(),
					"login.json",
					this,
					CallType.POST
						);
		
		ArrayList<RestParameter> params = new ArrayList<RestParameter>();
		params.add(new RestParameter("user", user));
		params.add(new RestParameter("passwd", password));

        RestParameter[] data = new RestParameter[params.size()];
        data = params.toArray(data);
		task.execute(data);
	}

    /**
     * Método que simplemente nos dirá si existe un token
     * dentro de las preferencias compartidas.
     * @return
     */
	@Override
	public Boolean IsUserLoggedIn() {
		String token = this.GetToken();

        return !token.isEmpty();
	}

	/**
	 * Métodos implementados de la interfaz IRestCallTaskCallback.
	 * De esta manera el objeto actuará como callback.
	 */
	@Override
	public void Executed(Integer id, String result, Boolean error) {
		if (id == UserLoginBOCallType.VALIDATE.GetValue()){
			if (error){
				this.RaiseError("Se ha producido un error validando el usuario");
			} else {
				this.ProcessValidateResponse(result);
			}
		}
	}

    /**
     * Método para propagar un error hacia arriba que podremos llamar desde
     * cualquier otro método de la clase.
     * @param error
     */
	private void RaiseError(String error){
		if (this._callback != null)
			this._callback.Error(error);
	}

    /**
     * Método para validar la respuesta del login.
     * Extraemos el token de la respuesta.
     * @param result
     */
	private void ProcessValidateResponse(String result){
		try{
			JSONObject data = new JSONObject(result).getJSONObject("data");
			String token = data.getString("token");
			
			this.SaveToken(token);
			
			this._callback.LoggedIn(true);
		} catch (Exception excep){
			this.RaiseError("Se ha producido un error inesperado");
		}
	}
	
	/**
	 * Enumeración que controlará 
	 * los números que se van a pasar para
	 * identificar la tarea del servicio REST que
	 * estamos presentando.
	 */
	private enum UserLoginBOCallType{
		VALIDATE(1);
		
		Integer _id;
        UserLoginBOCallType(Integer id){
			this._id = id;
		}
		
		public Integer GetValue(){
			return this._id;
		}
	}

}
