package com.contoso.hellocasette.businessobjects;

import android.content.Context;
import android.util.Log;

import com.contoso.hellocasette.dal.CallType;
import com.contoso.hellocasette.dal.RestCallTask;
import com.contoso.hellocasette.dal.RestCallTask.IRestCallTaskCallback;
import com.contoso.hellocasette.dal.RestParameter;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Clase de objetos de negocio para realizar el registro del usuario
 * en la aplicación.
 */
public class UserRegisterBO extends BusinessObjectsBase
		implements IUserRegisterBO, IRestCallTaskCallback{

	public static String Token;
	IUserRegisterBOCallback _callback;
	
	public UserRegisterBO(Context context, IUserRegisterBOCallback callback){
        super(context);
		this._callback = callback;
	}

    /**
     * Método para llamar al registro del usuario.
     * @param name
     * @param surname
     * @param password
     * @param repassword
     * @param email
     * @param remail
     * @param age
     */
	@Override
	public void Register(String name, String surname, String password,
			String repassword, String email, String remail, String age) {
		if (!email.equals(remail)){
			this.RaiseError("Email and repeat email field must match");
			return;
		}
		
		if (!password.equals(repassword)){
			this.RaiseError("Password and repeat password fields must match");
			return;
		}

		ArrayList<RestParameter> params = new ArrayList<RestParameter>();
		
		params.add(new RestParameter("name", name));
		params.add(new RestParameter("surname", surname));
		params.add(new RestParameter("email", email));
		params.add(new RestParameter("passwd", password));
		params.add(new RestParameter("age", age));

        RestParameter[] data = new RestParameter[params.size()];
        data = params.toArray(data);

		new RestCallTask(UserRegisterBOCallType.REGISTER.GetValue(),"users/user.json", this, CallType.POST)
			.execute(data);
	}

    /**
     * Método que propagará el error hacia la actividad
     * o clase llamante.
     */
	private void RaiseError(String error){
		if (this._callback != null)
			this._callback.ErrorOnRegistration(error);
	}

    /**
     * Método que se ejecutará cuando se haya ejecutado la tarea
     * REST a la que hayamosllamado.
     * @param id Identificador de la llamada
     * @param result Resultado en string de la llamada.
     * @param error Booleano indicando si ha existido un error
     */
	@Override
	public void Executed(Integer id, String result, Boolean error) {
        Log.d("mio", result);
        if (id == UserRegisterBOCallType.REGISTER.GetValue()) {
            if (error)
                this.RaiseError(result);
            else {
                try {
                    JSONObject data = new JSONObject(result).getJSONObject("data");
                    String token = data.getString("token");

                    this.SaveToken(token);

                    if (this._callback != null)
                        this._callback.Registered();
                } catch (Exception excep) {
                    this.RaiseError("Error en el registro");
                }
            }
        }
	}

    private enum UserRegisterBOCallType{
        REGISTER(1);

        Integer _id;
        UserRegisterBOCallType(Integer id){
            this._id = id;
        }

        public Integer GetValue(){
            return this._id;
        }
    }

}
