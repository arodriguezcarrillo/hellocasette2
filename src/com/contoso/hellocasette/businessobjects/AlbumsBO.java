package com.contoso.hellocasette.businessobjects;


import android.content.Context;

import com.contoso.hellocasette.crosscutting.Album;
import com.contoso.hellocasette.dal.CallType;
import com.contoso.hellocasette.dal.RestCallTask;
import com.contoso.hellocasette.dal.RestParameter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AlbumsBO extends BusinessObjectsBase
        implements IAlbumsBO, RestCallTask.IRestCallTaskCallback {

    IAlbumsBOCallback _callback;
    IAlbumBOCallback _albumCallback;

    public AlbumsBO(Context context, IAlbumsBOCallback callback){
        super(context);
        this._callback = callback;
    }

    public AlbumsBO(Context context, IAlbumBOCallback callback){
        super(context);
        this._albumCallback = callback;
    }

    @Override
    public void Albums() {
        ArrayList<RestParameter> params = new ArrayList<RestParameter>();
        params.add(new RestParameter("token", this.GetToken()));

        this.CallAlbumsRest(params);
    }

    @Override
    public void Albums(String search) {
        ArrayList<RestParameter> params = new ArrayList<RestParameter>();
        params.add(new RestParameter("token", this.GetToken()));
        params.add(new RestParameter("search", search));

        this.CallAlbumsRest(params);
    }

    @Override
    public void Albums(Integer artist) {
        ArrayList<RestParameter> params = new ArrayList<RestParameter>();
        params.add(new RestParameter("token", this.GetToken()));
        params.add(new RestParameter("artist", artist.toString()));

        this.CallAlbumsRest(params);
    }

    @Override
    public void Album(Integer id) {
        ArrayList<RestParameter> params = new ArrayList<RestParameter>();
        params.add(new RestParameter("token", this.GetToken()));
        params.add(new RestParameter("id", id.toString()));

        RestParameter[] data = new RestParameter[params.size()];
        data = params.toArray(data);

        new RestCallTask(
                AlbumsBOCallType.ALBUM.GetValue(),
                "albums/item.json",
                this,
                CallType.GET
        ).execute(data);
    }

    private void CallAlbumsRest(ArrayList<RestParameter> params){
        RestParameter[] data = new RestParameter[params.size()];
        data = params.toArray(data);

        new RestCallTask(
                AlbumsBOCallType.ALBUMS.GetValue(),
                "albums/all.json",
                this,
                CallType.GET
        ).execute(data);
    }

    @Override
    public void Executed(Integer id, String result, Boolean error) {
        if (error){
            if (this._callback != null)
                this._callback.ErrorLoading(result);

            if (this._albumCallback != null)
                this._albumCallback.ErrorLoading(result);
        } else {
            if (id == AlbumsBOCallType.ALBUMS.GetValue()){
                this.ProcessAlbumsResponse(result);
            } else if (id == AlbumsBOCallType.ALBUM.GetValue()){
                this.ProcessAlbumResponse(result);
            }
        }
    }

    private void ProcessAlbumsResponse(String response){
        try{
            ArrayList<Album> result = new ArrayList<Album>();

            JSONArray albums = new JSONObject(response).getJSONArray("data");

            Integer index = 0;
            while (index < albums.length()){
                JSONObject album = albums.getJSONObject(index);

                Album itemToAdd = new Album();
                itemToAdd.Id = album.getInt("id");
                itemToAdd.Name = album.getString("name");
                itemToAdd.Year = album.getInt("year");
                itemToAdd.Image = album.getString("image");
                result.add(itemToAdd);

                index++;
            }

            if (this._callback != null)
                this._callback.AlbumsReceived(result);

        } catch (Exception excep){
            if (this._callback != null)
                this._callback.ErrorLoading("Se ha producido un error cargando los datos de los albums");
        }
    }

    private void ProcessAlbumResponse(String response){
        try{
            JSONObject album = new JSONObject(response).getJSONObject("data");

            Album result = new Album();
            result.Id = album.getInt("id");
            result.Name = album.getString("name");
            result.Year = album.getInt("year");
            result.Image = album.getString("image");

            if (this._albumCallback != null)
                this._albumCallback.AlbumReceived(result);

        } catch (Exception excep){
            if (this._albumCallback != null)
                this._albumCallback.ErrorLoading("Se ha producido un error cargando los datos de los albums");
        }
    }

    public enum AlbumsBOCallType{
        ALBUMS(0), ALBUM(1);

        Integer _id;
        AlbumsBOCallType(Integer id){
            this._id = id;
        }

        public Integer GetValue() {
            return _id;
        }
    }
}
